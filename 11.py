#Zad 1
# class Student:
#     def __init__(self, imie, nazwisko, wiek, kierunek_studiow):
#         self.imie = imie
#         self.nazwisko = nazwisko
#         self.wiek = wiek
#         self.kierunek_studiow = kierunek_studiow
#
#     def wyswietl_dane(self):
#         print(f"Imie: {self.imie}\nNazwisko: {self.nazwisko}\nWiek: {self.wiek}\nStudia: {self.kierunek_studiow}")

#Zad 2
# class Pojazd():
#     def __init__(self, maks_predkosc, przebieg):
#         self.maks_predkosc = maks_predkosc
#         self.przebieg = float(przebieg)
#
#     def zwieksz_przebieg(self, przejechany_dystans):
#         self.przebieg += przejechany_dystans

#Zad 3
# class Prostokat:
#     def __init__(self, dlugosc, szerokosc):
#         self.dlugosc = dlugosc
#         self.szerokosc = szerokosc
#
#     def oblicz_pole(self):
#         self.pole = self.dlugosc * self.szerokosc
#         return self.pole
#
#     def oblicz_obwod(self):
#         self.obwod = 2 * self.dlugosc + 2 * self.szerokosc
#         return self.obwod

#Zad 4
# import random
# class Talia:
#     def __init__(self):
#         self.lista_kart = []
#
#     def shuffle(self):
#         losowa_karta = random.shuffle(self.lista_kart)
#         return losowa_karta
#     def deal(self):
#         ostatnia_karta = self.lista_kart.pop(-1)
#         return ostatnia_karta
#     def dodaj_karte(self, karta):
#         self.lista_kart.append(karta)
#
# class Karta:
#     def __init__(self, kolor, figura):
#         self.kolor = kolor
#         self.figura = figura
#     def pokaz_karte(self):
#         print(f"Kolor: {self.kolor}, figura: {self.figura}")
#
# talia = Talia()
# figury = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "jopek", "dama", "krol", "as"]
# kolory = ["pik","kier","trefl","karo"]
# for kolor in kolory:
#     for figura in figury:
#         nowa_karta = Karta(kolor, figura)
#         talia.dodaj_karte(nowa_karta)

#Zad 5

# class Manager():
#     def __init__(self):
#         self.zamowienia = {}
#     def dodaj_zamowienia(self, produkt, ilosc_produktu):
#         if produkt in self.zamowienia.keys():
#             self.zamowienia[produkt] += ilosc_produktu
#         else:
#             self.zamowienia.update({produkt: ilosc_produktu})
#     def sprzedaj_produkt(self, id_produktu):
#         for zamowienie in self.zamowienia:
#             if zamowienie.id == id_produktu:
#                 self.zamowienia[zamowienie] -= 1
#
# class Zamowienie():
#     def __init__(self, id, nazwa, cena):
#         self.id = id
#         self.nazwa = nazwa
#         self.cena = cena

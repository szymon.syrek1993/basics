###6.1
# Zad. 1

# lista_kolorow = ["zielony", "czerwony", "niebieski", "czarny", "fioletowy", "granatowy", "niebieski", "czarny",
#                  "czarny", "zielony", "cytrynowy", "granatowy", "niebieski", "indygo", "zielony", "czerwony"]
# zbior_kolorow = set(lista_kolorow)
#
# print(len(lista_kolorow))
# print(len(zbior_kolorow))
#
# for kolor in zbior_kolorow:
#     print(kolor)
#
# zbior_kolorow.add("bialy")
# print(zbior_kolorow)
#
# zbior_kolorow.remove("bialy")
# print(zbior_kolorow)

# Zad. 2

# lista_znakow_interpunkcyjnych = [",", ".", ";", "!", "?"]
#
# #wczytane_zdanie = input("Wprowadz dowolne zdanie: ")
# wczytane_zdanie = "da fames ac turpis egestas. Morbi scelerisque odio tellus, nec vulputate odio posuere at. Cras eleifend nibh lacinia vehicula congue. Praesent eu odio vitae ante dictum porttitor. Suspendisse potenti. Nunc eget libero volutpat quam bibendum scelerisque. Phasellus dapibus tempor nisi eu porta. Ut tempus orci at ex lacinia porttitor. Phasellus fermentum est metus, maximus eleifend libero suscipit ac. Cras rutrum velit vitae iaculis euismod. Quisque quis mattis mi. Quisque scelerisque porttitor lacus. Nam iaculis tellus at ipsum molestie finibus. Mauris mattis ultricies ex, in congue justo dictum a. Etiam egestas eget ex sit amet auctor."
#
# for znak in lista_znakow_interpunkcyjnych:
#     wczytane_zdanie.replace(znak, " ")
# lista_wyrazow = wczytane_zdanie.split(" ")
# tupla_wyrazow = tuple(lista_wyrazow)
#
# print(f"W zdaniu jest {len(tupla_wyrazow)} wyrazow")
# for wyraz in tupla_wyrazow:
#     print(wyraz, end=" ")
#
# print(f"\nPierwszy wyraz to:{tupla_wyrazow[0]}, czwarty wyraz to: {tupla_wyrazow[3]}")
#
# zbior_wyrazow = set(lista_wyrazow)
#
# print(f"\nW zdaniu jest {len(zbior_wyrazow)} unikatowych wyrazow")
#
# for wyraz in zbior_wyrazow:
#     print(wyraz, end=" ")

# Zbiory nie sa indeksowane stad nie mozna porownac wyrazow na konkretnych pozycjach, gdyz w zbiorze konkretnych pozycji nie ma

# Zad. 3

# moje_ulubione_kolory = {"zielony", "niebieski", "fioletowy"}
# wprowadzone_kolory = input("Wprowadz swoje ulubione kolory rozdzielone spacja: ")
# lista_kolorow = wprowadzone_kolory.split(" ")
# zbior_kolorow = set(lista_kolorow)
#
# if moje_ulubione_kolory == zbior_kolorow:
#     print("nasze ulubione kolory sa takie same")
# else:
#     print(f"nasze wspolne, ulubione kolory to: {moje_ulubione_kolory.intersection(zbior_kolorow)}")
#     print(f"kolory, ktore ty lubisz a ja nie: {zbior_kolorow.difference(moje_ulubione_kolory)}")
#     print(f"kolory, ktore lubie wylacznie ja to: {moje_ulubione_kolory.difference(zbior_kolorow)}")
#

# Zad. 4

# A = set()
# B = set()
# wartosc_graniczna = int(input("Wprowadz wartosc graniczna: "))
#
# for i in range(1, wartosc_graniczna):
#     if i % 2 == 0:
#         A.add(i)
#     if i % 3 == 2:
#         B.add(i)
#
# C = A | B
# D = A & B
# E = A - B
# F = B ^ A

###6.2

# Zad. 1

# albumy = {"The Sensual World": "Kate Bush",
#           "Shaday": "Ofra Haza",
#           "Achtung Baby": "U2",
#           "Aion": "Dead Can Dance",
#           "Invisible Touch": "Genesis"}

# print("Spis albumow:\n")
# for album in albumy.keys():
#     print(album)
#
# szukany_album = input("Wprowadz nazwe albumu: ")
# if szukany_album in albumy.keys():
#     print(f"Wykonawca albumu {szukany_album} jest {albumy[szukany_album]}. ")
# else:
#     print("Brak danych")

# Zad. 2

# wlacznik = True
# while wlacznik:
#     print(20*"*")
#     print("1. Wyswietl liste albumow")
#     print("2. Dodaj album")
#     print("3. Usun album")
#     print(20*"*")
#     opcja = input("Wybierz opcje: ")
#     if opcja == "1":
#         print("Spis albumow:\n")
#         for album in albumy.keys():
#             print(album)
#         szukany_album = input("Wprowadz nazwe albumu: ")
#         if szukany_album in albumy.keys():
#             print(f"Wykonawca albumu {szukany_album} jest {albumy[szukany_album]}. ")
#         else:
#             print("Brak danych")
#     elif opcja == "2":
#         nowy_album = input("Podaj nazwe albumy, ktory chcesz dodac: ")
#         wykonawca = input("Podaj wykonawce: ")
#         albumy.update({nowy_album: wykonawca})
#     elif opcja == "3":
#         album_do_usuniecia = input("Wprowadz album, ktory chcesz usunac: ")
#         albumy.pop(album_do_usuniecia)
#     elif opcja == "4":
#         wlacznik = False
#     else:
#         print("Brak wybranej opcji")

# Zad 3

# tekst = "Once upon a midnight dreary, while I pondered, weak and weary, Over many a quaint and curious " \
#         "volume of forgotten lore, While I nodded, nearly napping, suddenly there came a tapping, As of someone " \
#         "gently rapping, rapping at my chamber door. This visitor, I muttered, tapping at my chamber " \
#         "door - Only this, and nothing more."
# slownik = {}
# lista_znakow_interpunkcyjnych = [",", ".", ";", "!", "?"]
#
# for znak in lista_znakow_interpunkcyjnych:
#     tekst.replace(znak, " ")
# lista_wyrazow = tekst.split(" ")
# zbior_wyrazow = set(lista_wyrazow)
#
# for wyraz in zbior_wyrazow:
#     slownik.update({wyraz: tekst.count(wyraz)})


# Zad 4

# tekst = '''W polowie maja, juz przed wschodem slonca, o trzeciej zaczyna spiewac drozd, po nim rudzik, a chwile
# pozniej kos. Pol godziny pozniej odzywa sie kukulka. Zaraz po niej budzi sie bogatka. Wraz ze
# wschodem slonca, o czwartej godzinie, swoj koncert rozpoczynaja pleszka i zieba. Dwadziescia minut
# pozniej i wilga akcentuje swoja obecnosc wysoko w koronach drzew. Jeszcze pozniej swoje trzy grosze
# dodaje szpak, a tuz po nim kopciuszek. Najwiekszymi spiochami w tej ferajnie okazuja sie byc dzwoniec i szczygiel.
# '''
#
# slownik_nazw_ptakow = {'kos': 'Turdus merula', 'wilga': 'Oriolus oriolus', 'rudzik': 'Erithacus rubecula',
#                        'kukulka': 'Cuculus canorus', 'pleszka': 'Phoenicurus phoenicurus', 'bogatka': 'Parus major',
#                        'drozd': 'Turdus philomelos', 'zieba': 'Fringilla coelebs', 'dzwoniec': 'Chloris chloris',
#                        'szczygiel': 'Carduelis carduelis', 'szpak': 'Sturnus vulgaris',
#                        'kopciuszek': 'Phoenicurus ochruros'}
#
# for nazwa_pl, nazwa_lac in slownik_nazw_ptakow.items():
#     if nazwa_pl in tekst:
#         pelna_nazwa = nazwa_pl + f" ({nazwa_lac})"
#         tekst = tekst.replace(nazwa_pl, pelna_nazwa)
# print(tekst)


###6.3

# #Zad. 1
# wczytane_zdanie = '''W polowie maja, juz przed wschodem slonca, o trzeciej zaczyna spiewac drozd (Turdus philomelos), po nim rudzik (Erithacus rubecula), a chwile
# pozniej kos (Turdus merula). Pol godziny pozniej odzywa sie kukulka (Cuculus canorus). Zaraz po niej budzi sie bogatka (Parus major).'''
# lista_znakow_interpunkcyjnych = [",", ".", ";", "!", "?", "(", ")"]
# #wczytane_zdanie = input("Wprowadz dowolne zdanie: ")
# for znak in lista_znakow_interpunkcyjnych:
#     wczytane_zdanie.replace(znak, " ")
# lista_wyrazow = wczytane_zdanie.split(" ")
#
# for i in range(len(lista_wyrazow),0,-1):
#     print(lista_wyrazow[i-1])

#Zad. 2
# import random
#
# wynik = [12,1,45,76,50,23]
#
# for i in range(len(wynik)):
#     if wynik[i] < 1 or wynik[i] > 49:
#         print(f"Znaleziono bledna wartosc: {wynik[i]}")
#         poprawiony_numer =  random.randint(1, 49)
#         wynik[i] = poprawiony_numer
#         print(f"Nowa wartosc to: {poprawiony_numer}")

#Zad 3

# lista1 = ["abc", "def", "ghi", "jkl"]
# lista2 = [1, 2, 3, 4, 5]
# lista3 = ["xyz", 1, '2']
#
# print(lista1)
# print(lista2)
# print(lista3)
# print(f"pierwszy element: {lista1[0]}, czwaty element: {lista1[3]}")
# lista2[1] = lista3[1]
# lista3[2] = input("Wprowadz wartosc: ")
# lista1.append("slowo")
# del lista1[2]
# len(lista3)
# lista1.extend(lista3)

#Zad 4

# lista_znajomych = []
# wlacznik = True
#
# while wlacznik:
#     wprowadzony_wyraz = input("Wpisz imie znajomego, by zatrzymc program wpisz 'stop'")
#     if wprowadzony_wyraz == "stop":
#         wlacznik = False
#     else:
#         lista_znajomych.append(wprowadzony_wyraz)
# for imie in lista_znajomych:
#     print(f"Czesc {imie}! Dobrze Cie widziec!")

#Zad 5

# wczytane_zdanie = '''W polowie maja, juz przed wschodem slonca, o trzeciej zaczyna spiewac drozd (Turdus philomelos), po nim rudzik (Erithacus rubecula), a chwile
# pozniej kos (Turdus merula). Pol godziny pozniej odzywa sie kukulka (Cuculus canorus). Zaraz po niej budzi sie bogatka (Parus major).'''
# lista_znakow_interpunkcyjnych = [",", ".", ";", "!", "?", "(", ")"]
# #wczytane_zdanie = input("Wprowadz dowolne zdanie: ")
# for znak in lista_znakow_interpunkcyjnych:
#     wczytane_zdanie.replace(znak, " ")
# lista_wyrazow = wczytane_zdanie.split(" ")

# print(len(lista_wyrazow))
# licznik = 0
# for wyraz in lista_wyrazow:
#     if wyraz.istitle():
#         print(wyraz)
#         licznik += 1
# if licznik == 0:
#     print("Brak wyrazow zaczynajacych sie duza litera")

# licznik_2 = 0
# lista_szukanych_wyrazow = ["i", "w", "na", "pod", "dla"]
# for wyraz_2 in lista_szukanych_wyrazow:
#     for i in range(len(lista_wyrazow)):
#         if wyraz_2 == lista_wyrazow[i]:
#             print(i)
#             licznik_2 += 1
# if licznik_2 == 0:
#     print("W tekscie nie wystepuja wyrazy z listy")
#
# print(lista_wyrazow.sort(reverse=True)) # Z jakiegos powodu zwraca None

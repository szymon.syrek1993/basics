#Zad 1

# a = [1, 2, 3, 4, 5, 6, 7, 8]
# b = ['a', 'b', '7']
#
# for element in a:
#     if element in b:
#         print("listy maja co najmniej jeden wspolny element")

#Zad 2
# import random
#
# zbior_wartosci = set()
# for i in range(15):
#     zbior_wartosci.add(random.randint(5, 120))
#
# zbior_liczb_nieparzystych = zbior_wartosci.copy()
# for element in zbior_wartosci:
#     if element % 2 == 0:
#         zbior_liczb_nieparzystych.discard(element)

#Zad 3

# podstawowy_slownik = {'a' : 3, 'b' : 1, 'c' : 10, 'd' : 15, 'e' : 20}
# odwrocony_slownik = {}
#
# for klucz, wartosc in podstawowy_slownik.items():
#     odwrocony_slownik.update({wartosc: klucz})

#Zad 4

# pogoda_na_swiecie = {}
# pogoda_w_miescie = input("Podaj pogode w Twoim miescie: ")
#
# while pogoda_w_miescie != "":
#     miasto, temperatura = pogoda_w_miescie.split()
#     if miasto not in pogoda_na_swiecie.keys():
#         pogoda_na_swiecie.update({miasto: temperatura})
#     pogoda_w_miescie = input("Podaj pogode w Twoim miescie: ")
#
# for klucz, wartosc in pogoda_na_swiecie.items():
#     print(f"{klucz}: {wartosc}")

#Zad 5

# elementy_rachunku = [
#     ['Tom', 'Calamari', 6.00],
#     ['Tom', 'American Hot', 11.50],
#     ['Tom', 'Chocolate Fudge Cake', 4.45],
#     ['Clare', 'Bruschetta Originale', 5.35],
#     ['Clare', 'Fiorentina', 10.65],
#     ['Clare', 'Tiramisu', 4.90],
#     ['Rich', 'Bruschetta Originale', 5.35],
#     ['Rich', 'La Reine', 10.65],
#     ['Rich', 'Honeycomb Cream Slice', 4.90],
#     ['Rosie', 'Garlic Bread', 4.35],
#     ['Rosie', 'Veneziana', 9.40],
#     ['Rosie', 'Tiramisu', 4.90],
# ]
#
# rachunek = dict()
#
# for pozycja in elementy_rachunku:
#     gosc = pozycja[0]
#     potrawa = pozycja[1]
#     cena = pozycja[2]
#     if gosc not in rachunek.keys():
#         rachunek.update({gosc: {'potrawy': [potrawa], 'cena': cena}})
#     else:
#         rachunek[gosc]['potrawy'].append(potrawa)
#         rachunek[gosc]['cena'] += cena
# print(rachunek)
#Zad 1
# C:\przyklad.txt - sciezka bezwzgledna
# \katalog\przyklad.txt - sciezka wzgledna
# C:\outer_dir\inner_dir\przyklad.txt - sciezka bezwzgledna
# \outer_dir\innedr_dir\przyklad.txt - sciezka wzgledna

#Zad 2
# file = open("dane.txt", "r")
# lines = file.readlines()
# print(lines)
# W powyzszym przykladzie dokument zostaje wczytany w calosci do listy, po czym zostaje printowany w postaci listy
# ale nie jestem pewien, czy to blad, byc moze takie bylo zamierzenie.

#Zad 3

# new_file = open("przyklad.txt", "w")
# new_file.writelines(["Litwo, Ojczyzno moja! ty jesteś jak zdrowie;\n",
#                  "Ile cię trzeba cenić, ten tylko się dowie,\n",
#                  "Kto cię stracił. Dziś piękność twą w całej ozdobie\n"])
# new_file.close()
#
# file = open("przyklad.txt", "r")
# lines = file.readlines()
# for index in range(len(lines)):
#     if index % 2 == 0:
#         print(lines[index])

#Zad 4
# Kodowanie ASCII jest ograniczone ze wzgledu na swoja pojedmnosc, umozliwia zakodowanie tylko 128 znakow, powstalo
# na bazie jezyka angielskiego, nie posiada wiec chociazby polskich znakow. Kodowanie utf-8 znacznie rozszerza liczbe
# dostepnych symboli, proba wczytania pliku z polskimi znakami bez zmiany dekodowania skutkuje bledem.

# Zad 5
# with open("przyklad.txt", encoding="utf-8") as plik:
#     print(plik.tell())
#     plik.seek(43)
#     print(plik.read(1))
# Z jakiegos powodu zwracane jest 'o', ale nie rozumiem dlaczego skoro 'kursor' znajduje sie na ostatni wyrazie

#Zad 6
# with open("test.txt", "r") as plik:
#     linie = plik.readlines()
#     print(linie[3])
#

#Zad 7
# with open("przyklad.txt", "r+") as file:
#     text = file.read().replace("\n", " ")
#     words = text.split()
#     text_without_repeats = words[0]
#     for i in range(1, len(words)):
#         if words[i-1] != words[i]:
#             text_without_repeats += f" {words[i]}"
#

# #Zad 8
# import json
# def save_as_dict(**kwargs):
#     new_dict = {}
#     for key, val in kwargs.items():
#         new_dict.update({val: key})
#     return new_dict
# nowy_slownik = save_as_dict(klucz1="wartosc1", klucz2="wartosc2")
# with open('output.json', 'w') as fp:
#     json.dump(nowy_slownik, fp)
#
# #Zad 9
#
# with open('data.json', 'r') as fp:
#     data = json.load(fp)
# #print(data["imdata"][0]["l1PhysIf"]["attributes"].keys())
#
# print("Interface Status")
# print("==========================================================================")
# print("DN                                          Description       Speed  MTU  ")
# print("------------------------------------------- ----------------- ------ -----")
#
# for element in data["imdata"]:
#
#     package = element["l1PhysIf"]["attributes"]
#     dn = package["dn"]
#     mtu = package["mtu"]
#     descr = package["descr"]
#     speed = package["speed"]
#     print(f'{dn:<45}{descr:<20}{speed:<8}{mtu:<5}')

# #######################################################################
# with open("dane.txt", "r") as data, open("wyniki6.txt", "w") as asnwers:
#     # importing brightness values, dividing it to lines and converting strings to int values
#     brightness_values = []
#     list_of_lines = data.readlines()
#     for line in list_of_lines:
#         # brightness_values.append(line.rstrip().split(" "))
#         # brightness_values = map(int, brightness_values)
#         line = map(int, line.rstrip().split(" "))
#         brightness_values.append(list(line))

    # ############################################
    # #computing max and min values for brightness
    # max_brightness = 0
    # min_brightness = 255

    # for line_ in brightness_values:
    #     temp_max = max(line_)
    #     temp_min = min(line_)
    #     if temp_max > max_brightness:
    #         max_brightness = temp_max
    #     if temp_min < min_brightness:
    #         min_brightness = temp_min
    # print(max_brightness, min_brightness)

    # asnwers.write(f"Zad. 1 \nNajjasniejszy pixel = {max_brightness}, najciemniejszy = {min_brightness}\n")

    # ##############################################
    # #computing number of rows that are not symmetrical
    # ns_row_counter = 0
    # for line_ in brightness_values: # Iterating through each line
    #     val = 0
    #     for index in range(len(line_)//2): # Iterate through pairs of elements equally shifted from left and right "side"
    #         if line_[index] == line_[-index-1]: #Checks if elements are equall
    #             continue
    #         else:
    #             val += 1 #Counts how many times pair of elements in line was not equal
    #     if val != 0:
    #         ns_row_counter += 1 #Save information about no. rows being nonsymmetrical
    # asnwers.write(f"Zad. 2\nAby obraz posiadal pionowa os symetrii, nalezy usunac {ns_row_counter} wierszy.\n")

    ################################################
    # def return_neighbours(picture, row, col):
    #     neighbours = []
    #     try:
    #         neighbours.append(picture[row-1][col])
    #     except IndexError:
    #         pass
    #     try:
    #         neighbours.append(picture[row+1][col])
    #     except IndexError:
    #         pass
    #     try:
    #         neighbours.append(picture[row][col-1])
    #     except IndexError:
    #         pass
    #     try:
    #         neighbours.append(picture[row][col+1])
    #     except IndexError:
    #         pass
    #     return neighbours

    # counter = 0
    # for row, line_ in enumerate(brightness_values):
    #     for col, value in enumerate(line_):
    #         is_contrast = False
    #         neighbours = return_neighbours(brightness_values, row, col)
    #         for neighbour in neighbours:
    #             if abs(neighbour - value) > 128:
    #                 is_contrast = True
    #         if is_contrast:
    #             counter += 1

    # asnwers.write(f"Zad. 3\nW sygnale znajduje sie: {counter} sasiadujacych pixeli, ktore kontrastuja.\n")

    # Zad 4
    # pass

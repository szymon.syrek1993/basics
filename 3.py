# '''Zad 1 (W tresci zadania jest blad -  nie ma twierdzenia, ktore by mowilo,ze w trojkacie prostokatnym
# suma kwadratow dwoch dowolnych bokow jest rowna kwadratowi dlugosci trzeciego boku '''
#
# dlugosc_1_boku = int(input("Podaj dlugosc pierwszego boku: "))
# dlugosc_2_boku = int(input("Podaj dlugosc drugiego boku: "))
# dlugosc_3_boku = int(input("Podaj dlugosc trzeciego boku: "))
#
# if dlugosc_1_boku ** 2 + dlugosc_2_boku ** 2 == dlugosc_3_boku ** 2:
#     print("Trojkat jest prostokatny.")
# else:
#     print("Trojkat nie jest prostokatny.")

#Zad 2.

# wprowadzona_liczba = int(input("Wprowadz dowolna liczbe: "))
#
# if wprowadzona_liczba > 0:
#     print("Liczba jest dodatnia.")
# elif wprowadzona_liczba == 0:
#     print("Liczba jest rowna 0.")
# else:
#     print("Liczba jest ujemna.")

#Zad. 3

# pierwsza_liczba = int(input("Podaj pierwsza liczbe: "))
# druga_liczba = int(input("Podaj druga liczbe: "))
#
# if druga_liczba > pierwsza_liczba:
#     najwyzsza_liczba = druga_liczba
# else:
#     najwyzsza_liczba = pierwsza_liczba
#
# trzecia_liczba = int(input("Podaj trzecia liczbe: "))
#
# if trzecia_liczba > najwyzsza_liczba:
#     print(trzecia_liczba)
# else:
#     print(najwyzsza_liczba)

#Zad. 4

# wybor_1_uzytkownika = input('Wybierz kamien/papier/nozyce: ')
# wybor_2_uzytkownika = input('Wybierz kamien/papier/nozyce: ')
# if wybor_1_uzytkownika not in ['kamien', 'papier', 'nozyce'] or wybor_2_uzytkownika not in ['kamien', 'papier', 'nozyce']:
#     print('Bledne dane')
# else:
#     if wybor_1_uzytkownika == 'kamien':
#         if wybor_2_uzytkownika == 'kamien':
#             print('Remis')
#         elif wybor_2_uzytkownika == 'papier':
#             print('Wygrywa uzytkownik 2')
#         else:
#             print('Wygrywa uzytkownik 1')
#     elif wybor_1_uzytkownika == 'papier':
#         if wybor_2_uzytkownika == 'kamien':
#             print('Wygrywa uzytkownik 1')
#         elif wybor_2_uzytkownika == 'papier':
#             print('Remis')
#         else:
#             print('Wygrywa uzytkownik 2')
#     else:
#         if wybor_2_uzytkownika == 'kamien':
#             print('Wygrywa uzytkownik 2')
#         elif wybor_2_uzytkownika == 'papier':
#             print('Wygrywa uzytkownik 1')
#         else:
#             print('Remis')

#Zad 5

# pierwsza_liczba = int(input("Wprowadz pierwsza liczbe: "))
# druga_liczba = int(input("Wprowadz druga liczbe: "))
#
# if pierwsza_liczba % 2 == 0 or druga_liczba % 2 == 0:
#     print("Co najmniej jedna z liczb jest parzysta")
# else:
#     print("Zadna z liczb nie jest parzysta")

#Zad 6
# import random
#
# punktacja_gracz = 0
# punktacja_komputer = 0
#
# wybrana_strona = input("Wybierz orla (o) lub reszke (r) ")
# print(3)
# print(2)
# print(1)
# los = random.randint(0,1)
#
# if wybrana_strona == 'r':
#     if los == 0:
#         punktacja_komputer += 1
#     else:
#         punktacja_gracz += 1
# else:
#     if los == 0:
#         punktacja_gracz += 1
#     else:
#         punktacja_komputer += 1

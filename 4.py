#Zad 1

# koniec_przedzialu = int(input('Podaj koniec przedzialu: '))
#
# for i in range(koniec_przedzialu + 1):
#     print(i)
#
# licznik = 0
# while licznik <= koniec_przedzialu:
#     print(licznik)
#     licznik += 1

#Zad 2

# pierwsza_liczba = 100
# ostatnia_liczba = 50
#
# for i in range(pierwsza_liczba, ostatnia_liczba - 1 , -1):
#     print(i)
#
# while pierwsza_liczba >= ostatnia_liczba:
#     print(pierwsza_liczba)
#     pierwsza_liczba -= 1

#Zad 3

# for i in range(0, 105, 5):
#     print(i)

#Zad 4

# rzad_potegi = int(input('Wprowadz rzad potegi: '))
#
# for i in range(rzad_potegi):
#     print(2**i)

#Zad 5

# poczatek_przedzialu = int(input('Podaj poczatek przedzialu: '))
# koniec_przedzialu = int(input('Podaj koniec przedzialu: '))
# dzielnik = int(input('Podaj dzielnik: '))
#
# for i in range(poczatek_przedzialu, koniec_przedzialu + 1):
#     if i % dzielnik == 0:
#         print(i)

#Zad 6 (nie do konca ogarniam to zadanie )

# suma = 0
# wprowadzona_liczba = int(input("Wprowadz liczbe: "))
#
# while suma < suma + wprowadzona_liczba:
#     suma += wprowadzona_liczba
#     wprowadzona_liczba = int(input("Wprowadz liczbe: "))
# suma += wprowadzona_liczba
# print(suma)

#Zad 7

#print('*' * 10)

# for i in range(1, 5):
#     print('*' * i)

# for i in range(3):
#     print('*' * 3)

# for i in range(1, 11, 2):
#     pietro_choinki = '*' * i
#     print(pietro_choinki.center(9, ' '))

#Zad 8

# suma = 0
#
# for i in range (1, 11):
#     suma += i
# srednia = suma / 10
# print(f'Srednia: {srednia}')

#Zad 9

# poziom_paliwa = int(input('Podaj poczatkowa ilosc paliwa: '))
# while poziom_paliwa < 5000 or poziom_paliwa > 30000:
#     poziom_paliwa = int(input('Podaj prawidlowa wartosc poziomu paliwa: '))
#
# ilosc_astronautow = int(input('Podaj liczbe austronautow: '))
# while ilosc_astronautow <= 0 or ilosc_astronautow > 7:
#     ilosc_astronautow = int(input('Podaj liczbe austronautow: '))
#
# przebyty_dystans = 0
# while poziom_paliwa > 0:
#     poziom_paliwa -= 300 + 100 * ilosc_astronautow
#     przebyty_dystans += 100
#     print(f'Rakieta przebyla: {przebyty_dystans}, pozostalo paliwa: {poziom_paliwa}')
#
# if przebyty_dystans >= 2000:
#     print("Statek dotarl do orbity")
# else:
#     print("Statek nie dotarl do orbity")

#Zad 10

# badana_liczba = int(input('Wprowadz liczbe: '))
# zbior_dzielnikow = []
#
# for i in range(1, badana_liczba):
#     if badana_liczba % i == 0:
#         zbior_dzielnikow.append(i)
#
# if badana_liczba == (zbior_dzielnikow):
#     print('Badana liczba jest liczba doskonala')

#Zad 1

# dowolny_napis = "Nullam volutpat massa mi, quis sodales leo pellentesque in. "
# for i in range(10):
#     print(dowolny_napis)

#Zad 2

# suma = 0
# wprowadzona_liczba = int(input("Wprowadz liczbe: "))
#
# for i in range(1, wprowadzona_liczba + 1):
#     suma += i
# print(suma)

#Zad 3

# licznik = 0
# lista_liczb = []
#
# while licznik < 10:
#     wybrana_liczba = int(input("Wprowadz liczbe: "))
#     lista_liczb.append(wybrana_liczba)
#     licznik += 1
#
# for liczba in lista_liczb:
#     if liczba % 2 == 0:
#         print(liczba)

# Zad 4

# dni_tygodnia = ["poniedzialek", "wtorek", "sroda", "czwartek", "piatek", "sobota", "niedziela"]
#
# wybrana_liczba = int(input("Podaj liczbe: "))
#
# if wybrana_liczba not in [1, 2, 3, 4, 5, 6, 7]:
#     print("nie ma takiego dnia")
# else:
#     print(dni_tygodnia[wybrana_liczba-1])

#Zad 5

# d = (1, [2, 4], 'tekst', 3 + 5j)
#
# print(d[-1])
# print(d[:2])
#
#
# if 'abc' in d:
#     print("Tak")

#Zad 6

# a = [3, 1, 5, 7, 9, 2, 6]
#
# a[3] # wyswietla czwarty element listy
# a[1:4] # wyswietla elementy od 2 do czwartego
# a[3:] # wyswietla od czwartego do ostatniego elementu listy
# a[-3:] # wyswietla ostatnie 3 elementy listy
# a[:3] # zwraca pierwsze 3 elementy listy
# a[3:-1] # zwraca od czwartego do ostatniego elementu listy
# a[::2] # zwraca co drugi element listy
# a[5:2:-1] # zwraca elementy od szostego do czwartego
# sum(a) # zwraca sume wszystkich elelemtow
# 8 in a # sprawdza, czy liczba 8 jest jednym z elementow listy
# 4 not in a # sprawdza czy 4 nie jest jednym z elementow listy

#Zad 7

# wprowadzona_liczba = int(input("Wprowadz liczbe: "))
#
# if wprowadzona_liczba < 0:
#     wartosc_bezwzgledna = -wprowadzona_liczba
# else:
#     wartosc_bezwzgledna = wprowadzona_liczba

#Zad 8

# waga = float(input("Podaj swoja wage [kg]: "))
# wzrost = float(input("Podaj swoj wzrost [m]: "))
# BMI = waga / wzrost ** 2
#
# if BMI < 18.5:
#     print("Masz niedowage.")
# elif BMI <= 24:
#     print("Twoja waga jest normalna.")
# elif BMI <= 26.5:
#     print("Masz lekka nadwage.")
# elif 26.5 < BMI < 30:
#     print("Masz nadwage!")
# elif 30 <= BMI <= 35:
#     print("Nadwaga i otylosc I stopnia")
# elif 35 < BMI <= 40:
#     print("Nadwaga i otylosc I stopnia")
# else:
#     print("Nadwaga i otylosc III stopnia")

#Zad 9

# -False - koniunkcja
# -True - koniunkcja
# -False - alternatywa
# -True - alternatywa

#Zad 10

#Zad 11

# dlugosc_prostokata = 7
# wysokosc_prostokata = 6
#
# for i in range(wysokosc_prostokata):
#     print(dlugosc_prostokata * "*")


# for i in range(5):
#     if i in [0, 4]:
#         print("*****")
#     else:
#         print("*   *")

#nie mam pomyslu jak wykorzystac petle do drukowania rzedu
# for i in range(5):
#     liczba_spacji = 4 - i
#     liczba_gwiazdek = 2 * i + 1
#     poziom = liczba_spacji * ' ' + liczba_gwiazdek * '*'
#     print(poziom)

#Zad 12

# for i in range(97, 123):
#     print(chr(i))
#
# for j in range(90, 64, -1):
#     print(chr(j))

#Zad 13

# wlacznik = True
# slownik = {}
#
# while wlacznik:
#     print(20 * "*")
#     print("1. Dodaj slowo")
#     print("2. Znajdz definicje slowa")
#     print("3. Usun slowo z definicja z slownika")
#     print("4. Zakoncz program")
#     print(20 * "*")
#     opcja = input("Wybierz opcje: ")
#
#     if opcja == '1':
#         nowe_slowo = input("Podaj slowo: ")
#         definicja = input("Podaj definicje slowa: ")
#         slownik.update({nowe_slowo: definicja})
#     elif opcja == '2':
#         szukane_slowo = input("Podaj szukane slowo: ")
#         print(slownik.get(szukane_slowo, 'W slowniku nie ma takiego slowa'))
#     elif opcja == '3':
#         slowo_do_usuniecia = input("Podaj slowo, ktore chcesz usunac: ")
#         slownik.pop(slowo_do_usuniecia, None)
#     elif opcja == '4':
#         wlacznik = False
#     else:
#         print("Nie ma takiej opcji")

# Zad 14

# plus = [["  *  "], ["  *  "], ["*****"], ["  *  "], ["  *  "]]
# for rzad in plus:
#     print(rzad)

#Zad 15

# spis_ludzi = {
#     '91020210131' :{
#         'kolor oczu': 'niebieskie',
#         'imie': 'Piotr',
#         'nazwisko': 'Brzezinski',
#         'wiek': 3
#     },
#     '93020210282' :{
#         'kolor oczu': 'brazowe',
#         'imie': 'Jakub',
#         'nazwisko': 'Ujazdowski',
#         'wiek': 22
#     },
#     '94020210232' :{
#         'kolor oczu': 'zielone',
#         'imie': 'Mikolaj',
#         'nazwisko': 'Bolechowicki',
#         'wiek': 74
#     },
#     '95020210989' :{
#         'kolor oczu': 'szare',
#         'imie': 'Szymon',
#         'nazwisko': 'Kobylanski',
#         'wiek': 31
#     },
#     '96020210131' :{
#         'kolor oczu': 'niebieskie',
#         'imie': 'Pawel',
#         'nazwisko': 'Wieckowicki',
#         'wiek': 3
#     }
# }
#
# imiona_matek = ['Basia', 'Kasia', 'Zosia', 'Marysia', 'Krysia']
# indeks = 0
# for dane in spis_ludzi.values():
#     dane.update({'imie matki': imiona_matek[indeks]})
#     indeks += 1
#     print(dane)
#
# for pesel in list(spis_ludzi.keys()):
#     if pesel[-1] == '1':
#         spis_ludzi.pop(pesel)
#
# for pesel, dane in spis_ludzi.items():
#     print(pesel)
#     for cecha, wartosc in dane.items():
#         print(f"{cecha}: {wartosc}")
#     print(20 * '*')
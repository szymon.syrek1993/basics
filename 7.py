#Zad 1

# for i in range(1, 6):
#     print(i * "*")
# for j in range(4, 0, -1):
#     print(j * "*")

#Zad 2

# wprowadzony_wyraz = input("Wprowadz wyraz: ")
# if wprowadzony_wyraz == wprowadzony_wyraz[::-1]:
#     print("Wyraz jest palindromem")
# else:
#     print("Wyraz nie jest palindromem")

#Zad 3

# czlonkowie_rodziny = ["Adam", "Stanislaw", "Joanna", "Kornelia", "Kacper"]
#
# for i in range( len(czlonkowie_rodziny)):
#     for j in range(i+1, len(czlonkowie_rodziny)):
#         print(czlonkowie_rodziny[i], czlonkowie_rodziny[j])

#Zad 4
#
# lista_liczb = []
#
# for i in range(1000, 10000):
#     lista_liczb.append(i)
# print(lista_liczb)

#Zad 5

# zamowienia = {"Klient_1335" : {"nazwa_potrawy" : "rosół", "ocena" : 5, "rachunek" : 20.0},
#               "Klient_222" : {"nazwa_deseru": "lody waniliowe", "rachunek": 5.0}}
# for klient, zamowienie in zamowienia.items():
#     print(klient)
#     for pozycja, wartosc in zamowienie.items():
#         print(f"{pozycja}: {wartosc}")

#Zad 6

# czestotliwosc_cyfr = {}
# wprowadzona_liczba = input("Wprowadz liczbe: ")
# for cyfra in range(10):
#     czestotliwosc_cyfr.update({str(cyfra): wprowadzona_liczba.count(str(cyfra))})
#
# for cyfra, czestotliwosc in czestotliwosc_cyfr.items():
#     if czestotliwosc != 0:
#         print(f"{cyfra}: {czestotliwosc}")

#Zad. 7

# dlugosc_ciagu = int(input("Wprowadz dlugosc ciagu: "))
#
# elementy_ciagu = [0, 1]
#
# for i in range(2, dlugosc_ciagu+1):
#     elementy_ciagu.append(elementy_ciagu[i-2] + elementy_ciagu[i-1])
#
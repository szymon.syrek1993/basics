#Zad 1
# # Pierwsze rozwiazanie:
# nums = [4, 6, 8, 24, 12, 2]
# max_value = 0
# index = 0
# for i in range(len(nums)):
#     if nums[i] > max_value:
#         max_value = nums[i]
#         index = i
# print(index, nums[index])
#
# # Rozwiazanie z wykorzystaniem enumerate
# nums = [4, 6, 8, 24, 12, 2]
# max_value = 0
# index = 0
# for i, value in enumerate(nums):
#     if value > max_value:
#         max_value = value
#         index = i
# print(index, nums[index])

#Zad 2
# Parametry okresla sie w momencie definiowania funkcji, argumenty z kolei, to juz konkretne wartosci
# podawane podczas wywolywania funkcji.
# Default argument, tj. argumenty domyslne, to wartosci argumentow, ktore zostaly zadeklarowane podczas definiowania
# funkcji na wypadek, w ktorym nie zostana one podane podczas wywolania funkcji.
# Keyword arguments, tj argumenty nazwane zwiekszaja czytelnosc kodu w sposob jawny pokazujacy co dana zmienna
# przechowuje, umozliwiaja takze zmiane kolejnosci podawania argumentow.

#Zad 3

# def fizz_buzz(n):
#     if n % 3 == 0 and n % 5 == 0:
#         message = "FizzBuzz"
#     elif n % 3 == 0:
#         message = "Fizz"
#     elif n % 5 == 0:
#         message = "Buzz"
#     else:
#         message = n
#     return message

#Zad 4

# def multiply_numbers(*args):
#     product = 1
#     for number in args:
#         product *= number
#     return product

#Zad 5

# ev_numbers = [2, 4, 6, 8, 10, 12]
# unev_numbers = [1, 3, 5, 7, 9, 11]
#
# def mix_numbers(**kwargs):
#     mixed_list = []
#     for ev, unev in zip(kwargs["even_numbers"], kwargs["uneven_numbers"]):
#         mixed_list += [unev, ev]
#     return mixed_list
#
# print(mix_numbers(even_numbers=ev_numbers, uneven_numbers=unev_numbers))

#Zad 6
# import random
# unfiltered_numbers = random.sample(range(0, 1000), 10)
#
# def filter_small_numbers(list_of_numbers):
#     filtered_numbers = []
#     for num in list_of_numbers:
#         if 9 < num < 100:
#             filtered_numbers.append(num)
#     return filtered_numbers

#Zad 7

# def oblicz_kat(godzina, minuta):
#     temp = godzina * 5
#     if temp > minuta:
#         kat = 360 - (temp - minuta) * 6
#     else:
#         kat = (minuta - temp) * 6
#     return kat
#
# print(oblicz_kat(godzina=2, minuta=5))
#
